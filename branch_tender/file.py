#!/usr/bin/env python3
# Copyright 2022-2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>

from dataclasses import dataclass, field
from typing import Optional
import toml


from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class GitlabProjectConfig:
    server: str
    project: str


@dataclass
class ProjectChangesConfig:
    target_branch: str = "main"

    branch_names_to_mr: dict[str, Optional[int]] = field(default_factory=dict)

    @classmethod
    def from_changes_dict(cls, changes):
        branch_names_to_mr = {}

        branch_names = changes.get("branch_names")
        if branch_names:
            branch_names_to_mr.update({name: None for name in branch_names})

        with_mr = changes.get("branch_names_with_mr")
        if with_mr:
            branch_names_to_mr.update({name: mr for name, mr in with_mr.items()})

        return cls(
            target_branch=changes["target_branch"],
            branch_names_to_mr=branch_names_to_mr,
        )

    def to_dict(self):
        return {
            "target_branch": self.target_branch,
            "branch_names": [
                name for name, mr in self.branch_names_to_mr.items() if mr is None
            ],
            "branch_names_with_mr": {
                name: mr
                for name, mr in self.branch_names_to_mr.items()
                if mr is not None
            },
        }


@dataclass_json
@dataclass
class RepoConfig:
    local_location: str


@dataclass
class Config:
    repo: RepoConfig
    changes: ProjectChangesConfig
    gitlab: GitlabProjectConfig

    def to_toml(self, fp):
        d = {
            "repo": self.repo.to_dict(),
            "changes": self.changes.to_dict(),
            "gitlab": self.gitlab.to_dict(),
        }
        toml.dump(d, fp)

    @classmethod
    def load_file(cls, fn):
        data = toml.load(fn)
        repo = RepoConfig.from_dict(data["repo"])
        gitlab = GitlabProjectConfig.from_dict(data["gitlab"])
        changes_dict = data.get("changes")
        if changes_dict:
            changes = ProjectChangesConfig.from_changes_dict(changes_dict)
        else:
            changes = ProjectChangesConfig()
        return cls(repo=repo, gitlab=gitlab, changes=changes)
