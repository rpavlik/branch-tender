#!/usr/bin/env python3
# Copyright 2022-2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>


from dataclasses import dataclass
import os

import gitlab
import gitlab.v4.objects

from .file import GitlabProjectConfig


@dataclass
class GitlabProject:
    """Objects for interacting with a Gitlab repo."""

    gl: gitlab.Gitlab

    project: gitlab.v4.objects.Project

    @classmethod
    def inflate(cls, project_config: GitlabProjectConfig):
        from dotenv import load_dotenv

        load_dotenv()

        gl = gitlab.Gitlab(
            url=os.environ.get("GL_URL", project_config.server),
            private_token=os.environ["GL_ACCESS_TOKEN"],
        )

        project = gl.projects.get(project_config.project)
        return cls(gl=gl, project=project)
