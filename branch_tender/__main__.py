#!/usr/bin/env python3
# Copyright 2022-2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>

import logging

import click
import git

from .app import AppState
from .file import Config
from .gitlab_proj import GitlabProject


@click.group()
@click.argument("filename")
@click.option(
    "-v",
    "--verbose",
    is_flag=True,
    show_default=True,
    default=False,
    help="Verbose logging",
)
@click.pass_context
def cli(ctx, filename, verbose):
    if verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    ctx.ensure_object(dict)

    config = Config.load_file(filename)
    gitlabproj = GitlabProject.inflate(config.gitlab)

    repo = git.Repo(config.repo.local_location)
    state = AppState(repo, gitlabproj, config.changes)

    ctx.obj["filename"] = filename
    ctx.obj["config"] = config
    ctx.obj["state"] = state

    updated = state.lookup_merge_requests()
    if updated:
        click.echo(
            "Found new MR numbers, try running the 'update' subcommand if you are not already!"
        )


@cli.command()
@click.option(
    "--plain",
    is_flag=True,
    show_default=True,
    default=False,
    help="Plain text instead of markdown",
)
@click.pass_context
def topo_list(ctx, plain: bool):
    """Print a list of all merge requests, sorted topologically."""
    state: AppState = ctx.obj["state"]
    click.echo(state.topological_sort_mrs(not plain))


@cli.command()
@click.option(
    "--plain",
    is_flag=True,
    show_default=True,
    default=False,
    help="Plain text instead of markdown",
)
@click.pass_context
def mr_deps(ctx, plain: bool):
    """Print dependency lists for each open MR."""
    state = ctx.obj["state"]
    click.echo(state.gitlab_graph.mr_deps(state, not plain))


@cli.command()
@click.pass_context
def update(ctx):
    """Update the TOML file with MR numbers."""
    filename = ctx.obj["filename"]
    config = ctx.obj["config"]
    with open(f"{filename}", "w", encoding="utf-8") as fp:
        config.to_toml(fp)


if __name__ == "__main__":
    cli(obj={})
