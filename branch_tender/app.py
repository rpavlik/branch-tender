#!/usr/bin/env python3
# Copyright 2022-2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
from dataclasses import dataclass, field
import logging
from typing import cast
from functools import cached_property

import gitlab
import gitlab.v4.objects
import networkx as nx
import git

from .branches import BranchData
from .file import ProjectChangesConfig
from .gitlab_proj import GitlabProject

_LOG = logging.getLogger(__name__)


class AppState:
    def __init__(
        self,
        repo: git.Repo,
        gitlabproj: GitlabProject,
        proj_changes_config: ProjectChangesConfig,
    ):
        self._log = _LOG.getChild("AppState")
        self.proj_changes_config: ProjectChangesConfig = proj_changes_config
        self.gitlabproj = gitlabproj
        self.repo = repo
        self.project: gitlab.v4.objects.Project = gitlabproj.project
        self.branches: dict[str, BranchData] = {}

    def _populate_branch_mr(self, branch: BranchData):
        branch_name = branch.name
        mr_id = branch.mr_id
        if mr_id is None:
            self._log.debug("Looking up MR for branch name %s", branch_name)

            # Look for any MRs first
            mrs = list(
                self.project.mergerequests.list(source_branch=branch_name, all=True)
            )

            if len(mrs) > 1:
                # maybe only the opened ones?
                mrs = list(
                    self.project.mergerequests.list(
                        state=["opened"], source_branch=branch_name, all=True
                    )
                )

            if len(mrs) > 1:
                self._log.warning(
                    "Too many open merge requests for source branch %s: %d",
                    branch_name,
                    len(mrs),
                )
                return branch
            if not mrs:
                self._log.debug(
                    "No merge requests for source branch %s",
                    branch_name,
                )
                return branch
            branch.mr = cast(gitlab.v4.objects.ProjectMergeRequest, mrs[0])
        else:
            self._log.debug("Looking up MR %d branch name %s", mr_id, branch_name)
            branch.mr = self.project.mergerequests.get(mr_id)

    def lookup_merge_requests(self):
        """Fetch MR state and return True if we learned new MR IDs."""
        updated = False
        branch_names_config = self.proj_changes_config.branch_names_to_mr
        for name in branch_names_config.keys():
            if name not in self.branches:
                self.branches[name] = BranchData(name)
        for name, mr_id in branch_names_config.items():
            self._log.debug("Looking up %s", name)
            self._populate_branch_mr(self.branches[name])
            if mr_id is None:
                # We didn't know the MR ID before, but now we do.
                data = self.branches[name]
                if data.mr is not None:
                    mr_id = data.mr.get_id()
                    assert isinstance(mr_id, int)
                    branch_names_config[name] = mr_id
                    updated = True
        return updated

    @cached_property
    def gitlab_graph(self) -> "GitlabGraph":
        self._log.debug("In gitlab_graph")
        glg = GitlabGraph()
        G = glg.G
        G.add_node(self.proj_changes_config.target_branch)
        for name, data in self.branches.items():
            if data.mr is None:
                self._log.debug("Skipping branch name %s, no MR found", name)
                glg.no_mr.append(name)
                continue

            if data.mr.attributes["state"] != "opened":
                self._log.debug(
                    "Skipping MR %d branch name %s: state is %s",
                    data.mr_id,
                    name,
                    data.mr.attributes["state"],
                )
                glg.other_states.append(name)
                continue

            src = data.mr.attributes["source_branch"]
            tgt = data.mr.attributes["target_branch"]

            self._log.debug("MR %d: merging %s into %s", data.mr_id, src, tgt)
            G.add_edge(tgt, src, object=data)
        return glg

    def topological_sort_mrs(self, markdown=True):
        return self.gitlab_graph.topological_sort_mrs(self, markdown)


@dataclass
class GitlabGraph:
    G: nx.DiGraph = field(default_factory=nx.DiGraph)
    no_mr: list[str] = field(default_factory=list)
    other_states: list[str] = field(default_factory=list)

    def topological_sort_mrs(self, state: AppState, markdown=True):
        lines = []
        if self.no_mr:
            lines.append("- Branches with no MR")
            for branch_name in self.no_mr:
                lines.append(f"  - {branch_name}")
        if self.other_states:
            lines.append("- MRs in other states")
            for branch_name in self.other_states:
                branch = state.branches[branch_name]
                assert branch.mr is not None
                mr_id = branch.mr_id
                url = branch.mr.attributes["web_url"]
                title = branch.mr.attributes["title"]

                mr_state = branch.mr.attributes["state"]
                if markdown:
                    lines.append(f"  - [!{mr_id}]({url}): {title} - {mr_state}")
                else:
                    lines.append(f"  - {url} : {title} - {mr_state}")

        lines.append("- Open merge requests, sorted topologically, dependencies first")
        for tgt, src in nx.depth_first_search.dfs_edges(
            self.G, state.proj_changes_config.target_branch
        ):
            branch = state.branches[src]
            assert branch.mr is not None
            mr_id = branch.mr_id
            url = branch.mr.attributes["web_url"]
            title = branch.mr.attributes["title"]
            if markdown:
                lines.append(f"  - [!{mr_id}]({url}): {title} - {src} into {tgt}")
            else:
                lines.append(f"  - {url} : {title} - {src} into {tgt}")
        return "\n".join(lines)

    def mr_deps(self, state: AppState, markdown=True):
        lines = []
        tgt = state.proj_changes_config.target_branch

        def format_dep(b):
            branch = state.branches[b]
            assert branch.mr is not None

            mr_id = branch.mr_id
            url = branch.mr.attributes["web_url"]
            title = branch.mr.attributes["title"]
            if markdown:
                return f"- [!{mr_id}]({url}): {title}"

            return f"- {url} : {title}"

        for src in self.G.nodes:
            if src == tgt:
                # Not an MR, it's the overall target branch
                continue
            branch = state.branches[src]
            assert branch.mr is not None
            mr_id = branch.mr_id
            url = branch.mr.attributes["web_url"]
            title = branch.mr.attributes["title"]
            if markdown:
                lines.append(f"\n## Dependencies for [!{mr_id}]({url}): {title}\n")
            else:
                lines.append(f"\n\nDependencies for {url} : {title}")

            branch_path = nx.shortest_path(self.G, tgt, src)
            lines.extend(format_dep(b) for b in branch_path[1:-1])
            lines.append("- this MR")
            lines.append("\n")
        return "\n".join(lines)
