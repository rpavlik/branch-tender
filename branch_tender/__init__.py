#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Wrangle collections of related git branches more easily."""

__version__ = "0.1"
