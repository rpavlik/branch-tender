#!/usr/bin/env python3
# Copyright 2022-2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
from dataclasses import dataclass
from typing import Optional

import gitlab
import gitlab.v4.objects
import git


@dataclass
class BranchData:
    name: str
    mr: Optional[gitlab.v4.objects.ProjectMergeRequest] = None
    local_branch: Optional[git.Head] = None

    @property
    def mr_id(self):
        if self.mr is None:
            return None
        return self.mr.get_id()
